module.exports = {
  // 游戏根目录，如 D:/World of Warcraft 注意，这里的斜线需要是 '/' 而不是 '\'
  gameRootDir: 'C:/games/World of Warcraft',
  // "gameRootDir": "/Applications/World of Warcraft",
  // 单体插件下载目录
  downloadDir: './downloadDir',
  // 想要安装的插件
  plugins: [
    // "https://wow.curseforge.com/projects/altoholic",
    'https://wow.curseforge.com/projects/auctioneer',
    'https://wow.curseforge.com/projects/leatrix-plus',
    "https://wow.curseforge.com/projects/dejacharacterstats",
    'https://wow.curseforge.com/projects/deadly-boss-mods',
    'https://wow.curseforge.com/projects/dbm-voicepack-yike',
    'https://wow.curseforge.com/projects/details',
    'https://wow.curseforge.com/projects/master-plan',
    'https://wow.curseforge.com/projects/world-quest-tracker',
    'https://wow.curseforge.com/projects/pawn',
    'https://wow.curseforge.com/projects/spy',
    'https://wow.curseforge.com/projects/gladiatorlossa2',
    'https://wow.curseforge.com/projects/gladiatorlossa2_zhcn_female-vv',
    'https://www.wowace.com/projects/weakauras-2',
    'https://wow.curseforge.com/projects/omnibar',
    'https://wow.curseforge.com/projects/addonskins',
    'https://wow.curseforge.com/projects/nameplatecooldowns',
    'https://wow.curseforge.com/projects/mik-scrolling-battle-text',
    'https://wow.curseforge.com/projects/battlegroundenemies',
    'https://wow.curseforge.com/projects/diminish',
    'https://wow.curseforge.com/projects/azeritetooltip',
    'https://wow.curseforge.com/projects/championcommander',
    'https://wow.curseforge.com/projects/easy-scrap',
    'https://wow.curseforge.com/projects/omni-cc'
  ]
};
