twitch 貌似对 mac 支持不太友好，手动更新麻烦，于是就有了这个小项目 --- 魔兽世界单体插件更新器 nodejs 版。

### 下载安装 nodejs
[https://nodejs.org/en/](https://nodejs.org/en/) ， 建议选择 **LTS** 稳定版， 安装仅仅是一路下一步直到完成安装。

### 安装包
在本项目目录中输出以下命令。
```
npm install
```

### 配置
在 `config.js` 中可以看到我的配置，和一些我正在使用的插件，请按需增删改。
1. `config.js` 中配置游戏根目录 `gameRootDir`
2. `config.js` 中配置单体插件下载目录 `downloadDir`
3. `config.js` 中配置需要下载插件的 url `plugins`

### 更新
```
npm run start
```
PS: 魔兽插件一般是安装在 `游戏根目录/Interface/AddOns` 下面。

### 清空保存的信息和下载的缓存文件
```
npm run clear
```
PS: 删除 `downloadDir` 目录，和清空 `saveinfo.json` 中保存的信息。

### 备份用户设置
```
npm run backup
```
PS: 用户配置一般放在 `游戏根目录/WTF` 下面，此命令会把 `WTF` 目录复制到本项目下的 `WTF` 目录中。

### 恢复用户设置
```
npm run recover
```
PS: 备份用户设置的反向操作。原理是先删除 `游戏根目录/WTF` 然后在把本项目中的 `WTF` 目录复制过去。

### 更新失败
仆测试的时候失败次数最多的是**网络超时**，说明是网络的问题，这样就只能多试几次了。