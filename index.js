const https = require('follow-redirects').https,
  http = require('follow-redirects').http,
  colors = require('colors'),
  path = require('path'),
  fs = require('fs'),
  zlib = require('zlib'),
  saveInfo = require('./saveinfo.json'), // 保存信息
  config = require('./config.js'), // 配置文件
  cheerio = require('cheerio'),
  ProgressBar = require('progress'),
  // iconv = require('iconv-lite'),
  unzip = require('unzip'),
  del = require('del'),
  ncp = require('ncp').ncp,
  SocksProxyAgent = require('socks-proxy-agent'),
  Url = require('url');

var proxy = 'socks5://127.0.0.1:1080';
var agent = new SocksProxyAgent(proxy);

let count = 0;
function start() {
  if (fileExists(path.resolve(config.downloadDir)) !== 'dir') {
    fs.mkdirSync(path.resolve(config.downloadDir));
  }
  config.plugins.forEach(url => {
    getPage(url);
  });
}

function fileExists(path) {
  if (fs.existsSync(path)) {
    if (fs.statSync(path).isDirectory()) {
      return 'dir';
    } else {
      return 'file';
    }
  } else {
    return false;
  }
}

// function getProtocol(url) {
//   let protocol = null;
//   if (url.substr(0, 5).indexOf('https') > -1) {
//     protocol = https;
//   } else {
//     protocol = http;
//   }
//   return protocol;
// }
function getPage(url) {
  const opts = Url.parse(url);
  opts.agent = agent;
  var html = [];
  https
    .get(opts, page => {
      page.on('data', data => {
        html.push(data);
      });
      page.on('end', () => {
        html = Buffer.concat(html); // 1. 合并
        if (page.headers['content-encoding'] === 'gzip')
          html = zlib.gunzipSync(html); // 2. 是否需要解压缩
        /**
       * 针对性（网站）写法，不需要这些
      // 3. 是否需要解码
      let encoding = '';
      if (page.headers['content-type'].match(/charset/)) {
        encoding = page.headers['content-type'].match(/charset=(\w*-*\w*)/)[1].toLowerCase();
      } else {
        encoding = page.defaultEncoding;
      }
      switch (encoding) {
        case 'gbk':
          html = iconv.decode(html, 'gb2312');
          break;
        // case 'big5':
        //   html = iconv.decode(html, 'gb2312');
        //   break;
        default:
          break;
      }*/
        let $ = cheerio.load(html);
        let name = $('.overflow-tip')
          .eq(0)
          .html();
        let version = $('.overflow-tip')
          .eq(1)
          .html();
        if (checkUpdate(name, version)) {
          download(url + '/files/latest', name, version);
        }
      });
    })
    .on('error', err => {
      switch (err.code) {
        case 'ETIMEDOUT':
          console.log(`${url} 请求超时，重新发起请求。`.red);
          getPage(url);
        default:
          break;
      }
    });
}

function checkUpdate(name, version) {
  let isOutOfDate = false;
  if (!saveInfo.plugins[name] || saveInfo.plugins[name] !== version) {
    // 下载
    console.log(
      `${name} 当前版本为 ${
        saveInfo.plugins[name]
      }, 最新版本为 ${version}, 即将开始下载`.yellow
    );
    isOutOfDate = true;
    // saveInfo.plugins[name] = version;
  } else {
    isOutOfDate = false;
    count++;
    console.log(`${count}. ${name} 是最新版本，不需要更新`.green);
    if (count === config.plugins.length) console.log('插件更新完毕。'.green);
  }
  return isOutOfDate;
}

function download(url, name, version) {
  // let fullname = `${name}-${version}.zip`;
  const opts = Url.parse(url);
  opts.agent = agent;
  let fullname = `${name}.zip`;
  fullname = path.resolve(config.downloadDir, `${fullname}`);
  let file = fs.createWriteStream(fullname);
  https
    .get(opts, res => {
      let fileSize = res.headers['content-length'] * 1;
      let bar = new ProgressBar(name + ' [:bar] :percent', {
        complete: '=', // 完成的进度显示为等号
        incomplete: '_', // 待完成的进度显示为减号
        width: 50, // 设置显示的长度
        total: fileSize // 总进度条百分比
      });
      res.on('data', data => {
        bar.tick(data.length * 1); // 更新下载进度条
      });
      res.on('close', () => {
        unzipFile(fullname, name, version);
      });
      res.pipe(file);
    })
    .on('error', err => {
      switch (err.code) {
        case 'ETIMEDOUT':
          console.log(`${url} 下载请求超时，重新发起请求。`.yellow);
          download(url, name, version);
        default:
          break;
      }
    });
}

function unzipFile(file, name, version) {
  fs.createReadStream(file)
    .pipe(
      unzip.Extract({ path: config.gameRootDir + '/_retail_/Interface/AddOns' })
    )
    .on('close', function() {
      count++;
      console.log(`\r\n${count}. ${name}-${version}.zip 解压完毕`.green);
      saveInfo.plugins[name] = version;
      fs.writeFile(
        './saveinfo.json',
        JSON.stringify(saveInfo),
        { flag: 'w' },
        err => {
          if (err) throw err;
          if (count === config.plugins.length)
            console.log('插件更新完毕。'.green);
        }
      );
    });
}
function clear() {
  saveInfo.plugins = {};
  fs.writeFile(
    './saveinfo.json',
    JSON.stringify(saveInfo),
    { flag: 'w' },
    err => {
      if (err) throw err;
      console.log('清空缓存成功。'.green);
    }
  );
  del([config.downloadDir]).then(paths => {
    console.log('Deleted files and folders:\n', paths.join('\n'));
  });
}

function backup() {
  if (fileExists(path.resolve(`${config.gameRootDir}/_retail_/WTF`)) === 'dir') {
    ncp(`${config.gameRootDir}/_retail_/WTF`, './WTF', function(err) {
      if (err) {
        return console.log(err);
      }
      console.log('备份用户配置目录成功!'.green);
    });
  } else {
    console.log(`没有找到用户配置目录。`.yellow);
  }
}

function recover() {
  if (fileExists('./WTF') === 'dir') {
    del([`${config.gameRootDir}/_retail_/WTF`], { force: true }).then(paths => {
      ncp('./WTF', `${config.gameRootDir}/_retail_/WTF`, function(err) {
        if (err) {
          return console.error(err);
        }
        console.log('恢复用户配置目录成功!'.green);
      });
    });
  } else {
    console.log('没有找到用户配置目录。'.yellow);
  }
}

switch (process.env.NODE_ENV) {
  case 'update':
    start();
    break;
  case 'clear':
    clear();
    break;
  case 'backup':
    backup();
    break;
  case 'recover':
    recover();
    break;
  default:
    break;
}
